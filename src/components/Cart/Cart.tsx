import React from "react";
import { CartItemType } from "../../App";
import CartItem from "../CartItem/CartItem";
import { Wrapper } from "./Cart.styles";

export interface CartProps {
  cartItems: CartItemType[];
  addToCart: (clickeditem: CartItemType) => void;
  removeFromCart: (id: number) => void;
}
const Cart = ({ cartItems, addToCart, removeFromCart }: CartProps) => {
  const calculateTotal = (items: CartItemType[]) => {
    return items.reduce(
      (ack: number, item) => ack + item.amount * item.price,
      0
    );
  };
  return (
    <Wrapper>
      <h2>Your shopping cart</h2>
      {cartItems.length === 0 && <p>No items in cart</p>}
      {cartItems.map((item) => {
        return (
          <CartItem
            item={item}
            key={item.id}
            addToCart={addToCart}
            removeFromCart={removeFromCart}
          />
        );
      })}
      <h2>Total:$ {calculateTotal(cartItems).toFixed(2)}</h2>
    </Wrapper>
  );
};
export default Cart;
